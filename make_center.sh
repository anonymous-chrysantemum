#!/bin/sh

ANIMATION_DELAY=$(bc <<EOF
$(cat _inner_row_delay) - 0.2
EOF
		  )

cat <<EOF
      <div class="center" style="animation-delay: ${ANIMATION_DELAY}s;"></div>
EOF
