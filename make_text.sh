#!/bin/sh

ANIMATION_DELAY=$(bc <<EOF
$(cat _stem_delay) + 0.5
EOF
		  )

cat <<EOF
      <div class="wesolych" style="animation-delay: ${ANIMATION_DELAY}s;">
        <img src="wesolych.svg" alt="&quot;Wesołych Świąt&quot; text" draggabe="false"></img>
      </div>
      <div class="wszystkich" style="animation-delay: ${ANIMATION_DELAY}s;">
        <img src="wszystkich.svg" alt="&quot;Wszystkich Świętych&quot; text" draggable="false"></img>
      </div>
EOF
